#ifndef MELAHANDLER_H
#define MELAHANDLER_H

#include <yaml-cpp/yaml.h>

#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include <Mela.h>

#include <GMECHelperFunctions.h>
#include <MELACandidate.h>
#include <MELAEvent.h>


class MelaHandler {
  public:
    // Constructor
    MelaHandler(
        TTreeReader &reader,
        YAML::Node const &melaConfig,
        std::string const &higgsPoleMass,
        MELAEvent::CandidateVVMode VVMode,
        int VVDecayMode,
        float const melaHiggsMass = 125,
        float const sqrtS = 13,
        TVar::VerbosityLevel const vl = TVar::ERROR
        );

    // Destructor
    ~MelaHandler();

    // Provides the weight w.r.t. the given hyothesis name.
    float GetWeight(std::string const hypoName,
        std::string const hypoType, bool const finalWeights = true);

    // Gives the Higgs mass of the event.
    float GetHiggsMass();

    // Resets for a new computation for a new event.
    void Reset();

  private:
    // Gives the mass window factor w.r.t the Higgs mass in the event.
    float GetMassWindowFactor();

    // Builds the best candidate based on VV mode and VV decay mode
    void BuildBestCandidate();

    // Computes the ME for all clusters.
    bool ComputeClusters();
    
    // Computes Common Mela Clusters.
    void ComputeCommonClusters();

    // Computes NoInitialQ Mela Clusters.
    void ComputeNoInitialQClusters();

    // Computes NoInitialG Mela Clusters.
    void ComputeNoInitialGClusters();

    // Computes NoAssociatedG Mela Clusters.
    void ComputeNoAssociatedGClusters();

    // Computes BestNLOApproximationVBF Mela Clusters.
    void ComputeBestNLOVBFApproxClusters();

    // Computes BestNLO{Z,W}HApproximation Mela Clusters.
    void ComputeBestNLOVHApproxClusters(char const V);

    // Clears the mela object and all hypotheses, computers, and clusters.
    void Clear();

    static int objectNumber_;

    static std::unique_ptr<Mela> melaHandle_;

    bool computeMode_;

    std::vector<std::string> optionsList_; 

    std::vector<MELAHypothesis*> hypotheses_, aliasedHypos_;

    std::vector<MELAComputation*> computers_;

    std::vector<MELACluster*> clusters_, matchedClusters_;

    MELAEvent::CandidateVVMode VVMode_;

    int VVDecayMode_;

    float higgsMass_;

    MELACandidate* bestCandidate_;

    std::unique_ptr<MELAEvent> melaEvent_;

    std::vector<std::unique_ptr<MELAParticle>> stableParticles_, mothers_;

    TTreeReaderArray<float> ptArray_, etaArray_, phiArray_, massArray_;

    TTreeReaderArray<float> incomingPzArray_;

    TTreeReaderArray<int> pdgIdArray_, statusArray_;

    TTreeReaderValue<unsigned int> numPart_;

    float refSampleXsec_, branchingRatio_;

    std::vector<std::map<std::string, float>> massWinFactors_;

    std::map<std::string, float> weightsSum_, refSampleSF_;

};

#endif
