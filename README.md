# MELA

This repository provides tools to reweight the [NanoAOD](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD) POWHEG samples.


## Installation recipe

Please make sure that [MELA](https://github.com/JHUGen/JHUGenMELA/tree/master/MELA) and [MelaAnalytics](https://github.com/MELALabs/MelaAnalytics) packages are installed properly w.r.t the proper LCG environment:

Clone and build this repository:

If `MELA` and `MelaAnalytics` packages are not installed beside this repository folder please modify the `env.sh` w.r.t. their installed directories.

```sh
git clone ssh://git@gitlab.cern.ch:7999/mmahdavi/mela_reweighting.git 
. ./env.sh
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

## How to run:

```sh
mela --output=output.root --config=config/GG_SIG_ZZ_POWHEG.yaml --sample-hmass=SAMPLE_HIGGS_POLE_MASS INPUT_File_1 [INPUT_FILE_2 [... [INPUT_FILE_N]]]
```

