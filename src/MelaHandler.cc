#include <MelaHandler.h>

#include <filesystem>
#include <iostream>

#include <boost/algorithm/string/replace.hpp>

#include <MELACandidateRecaster.h>
#include <HiggsComparators.h>


namespace fs = std::filesystem;
namespace PDG = PDGHelpers;

int MelaHandler::objectNumber_;
std::unique_ptr<Mela> MelaHandler::melaHandle_{nullptr};

MelaHandler::MelaHandler(
    TTreeReader &reader,
    YAML::Node const &melaConfig,
    std::string const &higgsPoleMass,
    MELAEvent::CandidateVVMode VVMode,
    int VVDecayMode,
    float const melaHiggsMass,
    float const sqrtS,
    TVar::VerbosityLevel const vl
    )
    : computeMode_{true},
      optionsList_{},
      hypotheses_{},
      aliasedHypos_{},
      computers_{},
      clusters_{},
      matchedClusters_{},
      VVMode_{VVMode},
      VVDecayMode_{VVDecayMode},
      higgsMass_{0},
      bestCandidate_{nullptr},
      melaEvent_{nullptr},
      ptArray_{reader, "LHEPart_pt"},
      etaArray_{reader, "LHEPart_eta"},
      phiArray_{reader, "LHEPart_phi"},
      massArray_{reader, "LHEPart_mass"},
      incomingPzArray_{reader, "LHEPart_incomingpz"},
      pdgIdArray_{reader, "LHEPart_pdgId"},
      statusArray_{reader, "LHEPart_status"},
      numPart_{reader, "nLHEPart"} {

  objectNumber_++;

  if (not melaHandle_ and sqrtS > 0 and melaHiggsMass > 0)
    melaHandle_ = std::make_unique<Mela>(sqrtS, melaHiggsMass, vl);

  if (not melaConfig) {
    std::ostringstream message;
    message << "MELA configuration is empty.";
    throw std::runtime_error(message.str());
  }

  refSampleXsec_ = melaConfig["reference_cross_section"].as<float>();
  branchingRatio_ = melaConfig["branching_ratio"].as<float>();

  // Load mass window reweighting factors.
  auto tempCfg = YAML::LoadFile(
      "config/" + melaConfig["mass_window_factors"].as<std::string>());
  tempCfg = tempCfg[higgsPoleMass];

  std::map<std::string, float> massWinFactor;
  for (auto const &node : tempCfg) {
    massWinFactor["lower"] = node["lower"].as<float>();
    massWinFactor["upper"] = node["upper"].as<float>();
    massWinFactor["factor"] = node["factor"].as<float>();
    massWinFactors_.push_back(massWinFactor);
  }

  // Load weight sums.
  tempCfg = YAML::LoadFile(
      "config/" + melaConfig["weights_sum"].as<std::string>());
  tempCfg = tempCfg[higgsPoleMass];
  weightsSum_["SIG"] = tempCfg["SIG"].as<float>();
  weightsSum_["BSI"] = tempCfg["BSI"].as<float>();
  weightsSum_["BKG"] = tempCfg["BKG"].as<float>();

  // Load reference sample scale factors.
  tempCfg = melaConfig["reference_scale_factors"];
  refSampleSF_["SIG"] = tempCfg["SIG"].as<float>();
  refSampleSF_["BSI"] = tempCfg["BSI"].as<float>();
  refSampleSF_["BKG"] = tempCfg["BKG"].as<float>();

  // Load meps config and create the hypotheses and computations. 
  auto mepsConfigPath = "config/" + melaConfig["meps"].as<std::string>();
  YAML::Node mepsConfig = YAML::LoadFile(mepsConfigPath);
  std::string optStr;
  for (auto const &opt : mepsConfig) {
    optStr = opt.as<std::string>();
    boost::replace_all(optStr, "<HMASS>", higgsPoleMass);
    MELAHypothesis *hypo = new MELAHypothesis(melaHandle_.get(), optStr);
    MELAComputation *computer = new MELAComputation(hypo);
    hypotheses_.push_back(hypo);
    computers_.push_back(computer);
    GMECHelperFunctions::addToMELACluster(computer, clusters_);

    MELAOptionParser melaOption(optStr);
    if (melaOption.isAliased())
      aliasedHypos_.push_back(hypo);
  }

  for (auto computer: computers_)
    computer->addContingencies(aliasedHypos_);

  if (not clusters_.empty()) {
    std::cout << "ME clusters:" << std::endl;
    for (auto const cluster : clusters_) {
      std::cout << "\t- Cluster " << cluster->getName() <<
        " has " << cluster->getComputations()->size() <<
        " computations registered." << std::endl;
    }
  }

}


MelaHandler::~MelaHandler() {
  objectNumber_--;
  Clear();
}


float MelaHandler::GetWeight(std::string const hypoName,
    std::string const hypoType, bool const finalWeights) {
  if (computeMode_) {
    BuildBestCandidate();
    if (not bestCandidate_)
      return -1;

    melaHandle_->setCurrentCandidate(bestCandidate_);
    if (not ComputeClusters())
      return -1;

    computeMode_ = false;
  }

  float weight = 1, cpsToBw = -1;
  MELAHypothesis::METype valtype;
  if (hypoName.find("pAux") != std::string::npos)
    valtype = MELAHypothesis::UsePAux;
  else if (hypoName.find("pConst") != std::string::npos)
    valtype = MELAHypothesis::UsePConstant;
  else
    valtype = MELAHypothesis::UseME;

  for (auto computer : computers_) {
    if (computer->getName() == hypoName)
      weight = computer->getVal(valtype);
    if (computer->getName() == "CPStoBWPropRewgt")
      cpsToBw = computer->getVal(MELAHypothesis::UseME);
  }

  if (hypoName == "CPStoBWPropRewgt")
    return weight;

  if (not finalWeights)
    return weight;

  weight = weight * cpsToBw * refSampleXsec_ * branchingRatio_ *
    refSampleSF_[hypoType] * GetMassWindowFactor();
  weight /=  weightsSum_[hypoType];
  return weight;
}


float MelaHandler::GetHiggsMass() {
  if (higgsMass_ > 0)
    return higgsMass_;

  TLorentzVector p4, higgsP4;
  for (unsigned int i = 0; i < *numPart_.Get(); i++) {
    auto const &pt = ptArray_[i], &eta = etaArray_[i], &phi = phiArray_[i];
    auto const &mass = massArray_[i];
    auto const &pdgId = pdgIdArray_[i];

    p4.SetPtEtaPhiM(pt, eta, phi, mass);
    if (PDG::isALepton(pdgId) or PDG::isANeutrino(pdgId))
      higgsP4 += p4;
  }
  higgsMass_ = higgsP4.M();
  return higgsMass_;
}


void MelaHandler::Reset() {
  for (auto cluster : clusters_)
    cluster->reset();

  mothers_.clear();
  stableParticles_.clear();
  higgsMass_ = 0;
  computeMode_ = true;
}


float MelaHandler::GetMassWindowFactor() {
  higgsMass_ = GetHiggsMass();
  for (auto &i : massWinFactors_)
    if (higgsMass_ >= i["lower"] and higgsMass_ < i["upper"])
      return i["factor"];

  return 0;
}


void MelaHandler::BuildBestCandidate() {
  melaEvent_ = std::make_unique<MELAEvent>();
  TLorentzVector p4;
  for (unsigned int i = 0; i < *numPart_.Get(); i++) {
    auto const &pt = ptArray_[i], &eta = etaArray_[i], &phi = phiArray_[i];
    auto const &mass = massArray_[i], &incomingPz = incomingPzArray_[i];
    auto const &pdgId = pdgIdArray_[i];
    auto const &status = statusArray_[i];

    p4.SetPtEtaPhiM(pt, eta, phi, mass);

    if (status == -1) { // Consider it as a mother particle.
      p4.SetPxPyPzE(0, 0, incomingPz, std::fabs(incomingPz));
      mothers_.push_back(std::make_unique<MELAParticle>(pdgId, p4));
      mothers_.back()->genStatus = status;
    } else if (status == 1) {
      stableParticles_.push_back(std::make_unique<MELAParticle>(pdgId, p4));
      stableParticles_.back()->genStatus = status;
    }
  }

  // Add mother particles.
  for (auto &mother : mothers_)
    melaEvent_->addMother(mother.get());

  for (auto &particle : stableParticles_) {
    particle->addMother(mothers_[0].get());
    particle->addMother(mothers_[1].get());
  }

  // Add the particles into the melaEvent.
  for (auto &particle : stableParticles_) {
    if (PDG::isAKnownJet(particle->id) and
        not(PDG::isATopQuark(particle->id)))
      melaEvent_->addJet(particle.get());

    else if (PDG::isALepton(particle->id))
      melaEvent_->addLepton(particle.get());

    else if (PDG::isANeutrino(particle->id))
      melaEvent_->addNeutrino(particle.get());

    else if (PDG::isAPhoton(particle->id))
      melaEvent_->addPhoton(particle.get());
  }

  melaEvent_->constructVVCandidates(VVMode_, VVDecayMode_);
  melaEvent_->addVVCandidateAppendages();

  bestCandidate_ = HiggsComparators::candidateSelector(
      *melaEvent_,
      HiggsComparators::BestZ1ThenZ2ScSumPt,
      VVMode_
      ); 
}


bool MelaHandler::ComputeClusters() {
  bestCandidate_ = melaHandle_->getCurrentCandidate();
  if (not bestCandidate_)
    return false;

  ComputeCommonClusters();
  ComputeNoInitialQClusters();
  ComputeNoInitialGClusters();
  ComputeNoAssociatedGClusters();
  ComputeBestNLOVBFApproxClusters();
  ComputeBestNLOVHApproxClusters('Z');
  ComputeBestNLOVHApproxClusters('W');
  return true;
}


void MelaHandler::ComputeCommonClusters() {
  for (auto cluster : clusters_) {
    if (cluster->getName() != "Common")
      continue;
    cluster->computeAll();
    cluster->update();
  }
}


void MelaHandler::ComputeNoInitialQClusters() {
  matchedClusters_.clear();
  // Check if there is any "NoInitialQ" cluster.
  for (auto cluster : clusters_)
    if (cluster->getName() == "NoInitialQ")
      matchedClusters_.push_back(cluster);

  if (matchedClusters_.empty())
    return;

  // Assign 0 to the Ids of quark mothers.
  int numMothers = bestCandidate_->getNMothers();
  std::vector<int> mothersIds;
  for (int i = 0; i < numMothers; i++) {
    int &id = bestCandidate_->getMother(i)->id;
    mothersIds.push_back(id);
    if (PDGHelpers::isAQuark(id))
      id = 0;
  }

  for (auto cluster : matchedClusters_) {
    cluster->computeAll();
    cluster->update();
  }

  // Restoring the mothers Ids.
  for (int i = 0; i < numMothers; i++)
    bestCandidate_->getMother(i)->id = mothersIds[i];
}


void MelaHandler::ComputeNoInitialGClusters() {
  matchedClusters_.clear();
  // Check if there is any "NoInitialG" cluster.
  for (auto cluster : clusters_)
    if (cluster->getName() == "NoInitialG")
      matchedClusters_.push_back(cluster);

  if (matchedClusters_.empty())
    return;

  // Assign 0 to the Ids of gluon mothers.
  int numMothers = bestCandidate_->getNMothers();
  std::vector<int> mothersIds;
  for (int i = 0; i < numMothers; i++) {
    int &id = bestCandidate_->getMother(i)->id;
    mothersIds.push_back(id);
    if (PDGHelpers::isAGluon(id))
      id = 0;
  }

  for (auto cluster : matchedClusters_) {
    cluster->computeAll();
    cluster->update();
  }

  // Restoring the mothers Ids.
  for (int i = 0; i < numMothers; i++)
    bestCandidate_->getMother(i)->id = mothersIds[i];
}


void MelaHandler::ComputeNoAssociatedGClusters() {
  matchedClusters_.clear();
  // Check if there is any "NoAssociatedG" cluster.
  for (auto cluster : clusters_)
    if (cluster->getName() == "NoAssociatedG")
      matchedClusters_.push_back(cluster);

  if (matchedClusters_.empty())
    return;

  // Assign 0 to the Ids of associated gluons.
  int numAssociatedJets = bestCandidate_->getNAssociatedJets();
  std::vector<int> jetsIds;
  for (int i = 0; i < numAssociatedJets; i++) {
    int &jetId = bestCandidate_->getAssociatedJet(i)->id;
    jetsIds.push_back(jetId);
    if (PDGHelpers::isAGluon(jetId))
      jetId = 0;
  }

  for (auto cluster : matchedClusters_) {
    cluster->computeAll();
    cluster->update();
  }

  // Restoring the jets Ids.
  for (int i = 0; i < numAssociatedJets; i++)
    bestCandidate_->getAssociatedJet(i)->id = jetsIds[i];
}


void MelaHandler::ComputeBestNLOVBFApproxClusters() {
  matchedClusters_.clear();
  // Check if there is any "BestNLOVBFApproximation" cluster.
  for (auto cluster : clusters_)
    if (cluster->getName() == "BestNLOVBFApproximation")
      matchedClusters_.push_back(cluster);

  if (matchedClusters_.empty())
    return;

  MELACandidateRecaster recaster(TVar::JJVBF);
  MELACandidate modifiedCand, *modifiedCandPtr = &modifiedCand;
  recaster.copyCandidate(bestCandidate_, modifiedCandPtr);
  recaster.deduceLOVBFTopology(modifiedCandPtr);
  melaHandle_->setCurrentCandidate(modifiedCandPtr);

  for (auto cluster : matchedClusters_) {
    cluster->computeAll();
    cluster->update();
  }
}


void MelaHandler::ComputeBestNLOVHApproxClusters(char const V) {
  TVar::Production candScheme;
  std::string  clusterName;
  if (V == 'Z') {
    candScheme = TVar::Had_ZH;
    clusterName = "BestNLOZHApproximation";
  }
  else if (V == 'W') {
    candScheme = TVar::Had_WH;
    clusterName = "BestNLOWHApproximation";
  }
  else
    return;

  matchedClusters_.clear();
  // Check if there is any "BestNLO{Z,W}HApproximation" cluster.
  for (auto cluster : clusters_)
    if (cluster->getName() == clusterName)
      matchedClusters_.push_back(cluster);

  if (matchedClusters_.empty())
    return;

  // Search for the best associated Vector Boson.
  MELACandidateRecaster recaster(candScheme);
  MELACandidate modifiedCand, *modifiedCandPtr = &modifiedCand;
  MELAParticle *bestAV = MELACandidateRecaster::getBestAssociatedV(
      bestCandidate_, candScheme);
  if (bestAV) {
    recaster.copyCandidate(bestCandidate_, modifiedCandPtr);
    recaster.deduceLOVHTopology(modifiedCandPtr);
    melaHandle_->setCurrentCandidate(modifiedCandPtr);
  }

  else
    // No associated Vector Boson found.
    return;

  for (auto cluster : matchedClusters_) {
    cluster->computeAll();
    cluster->update();
  }
}


void MelaHandler::Clear() {
  for (auto hypo : hypotheses_)
    delete hypo;

  for (auto computer : computers_)
    delete computer;

  for (auto cluster : clusters_)
    delete cluster;

  hypotheses_.clear();
  computers_.clear();
  clusters_.clear();
  if (objectNumber_ == 0)
    melaHandle_.reset();
}

