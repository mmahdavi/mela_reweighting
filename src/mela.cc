#include <iostream>

#include <boost/program_options.hpp>

#include <yaml-cpp/yaml.h>

#include <TFile.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include <Mela.h>

#include <MelaHandler.h>


namespace po = boost::program_options;


int main(int argc, char *argv[]) {
  bool isVerbose;
  std::string configPath, output, sampleHMass;
  std::vector<std::string> inputs;
  MELAEvent::CandidateVVMode VVMode = MELAEvent::ZZMode;
  int VVDecayMode = 3, firstEvent, lastEvent;
  /* VVDecayModes in VVMode = ZZMode:
   * VVDecayMode =  0  -->  4l
   * VVDecayMode = +1  -->  4q
   * VVDecayMode = +2  -->  2l2q
   * VVDecayMode = +3  -->  2l2nu
   * VVDecayMode = +4  -->  2q2nu
   * VVDecayMode = +5  -->  4nu
   * VVDecayMode = -1  -->  Any
   * VVDecayMode = -2  -->  2l2X
   * VVDecayMode = -3  -->  2nu2X
   * VVDecayMode = -4  -->  2q2X
   */

  // Managing program options.
  po::options_description optionsDescription{"Options"};
  optionsDescription.add_options()
    ("help,h", "Help screen")
    ("output,o", po::value<std::string>(&output)->required(), "Output file")
    ("config,c", po::value<std::string>(&configPath)->required(),
     "Path to the MELA configuration file")
    ("sample-hmass,m", po::value<std::string>(&sampleHMass)->required(),
     "Higgs pole mass of the POWHEG sample")
    ("first-event,f", po::value<int>(&firstEvent)->default_value(1),
     "Fist event number")
    ("last-event,l", po::value<int>(&lastEvent)->default_value(-1),
     "Last event number")
    ("verbose,v", "Show progress");

  po::options_description hiddenOptionsDescription;
  hiddenOptionsDescription.add_options()
    ("input-files", po::value<std::vector<std::string>>(), "");
  po::positional_options_description posOptionsDescription;
  posOptionsDescription.add("input-files", -1);

  po::options_description allOptionsDescription;
  allOptionsDescription.add(optionsDescription).add(hiddenOptionsDescription);

  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(allOptionsDescription)
      .positional(posOptionsDescription).run(),
      options);

  if (options.count("help")) {
    std::cerr << "Usage:" << std::endl
      << "mela INPUT_FILE1 [INPUT_FILE2 [...]] [OPTIONS]" << std::endl;
    std::cerr << optionsDescription << std::endl;
    return EXIT_SUCCESS;
  }

  isVerbose = options.count("verbose");
  po::notify(options);

  // If no input file is provided throw an exception.
  if (options["input-files"].empty()) {
    std::ostringstream message;
    message << "No input file is provided. Please pass them as POSITIONAL "
      << "arguments.";
    throw std::runtime_error(message.str());
  }

  YAML::Node melaConfig = YAML::LoadFile(configPath);
  
  inputs = options["input-files"].as<std::vector<std::string>>();
  if( lastEvent == -1)
    lastEvent = INT_MAX;

  TFile outputFile(output.c_str(), "RECREATE");
  TTree tree("tree", "tree");
  tree.SetDirectory(&outputFile);

  // Adding input files to the chain.
  TChain chain("Events");
  TTreeReader reader(&chain);
  for (auto &file : inputs)
    chain.Add(file.c_str());

  if (isVerbose)
    std::cout << "Input root files are added into the chain." << std::endl;
  // Instantiate the MelaHandler before events iteration.
  MelaHandler melaHandler(
      reader,
      melaConfig,
      sampleHMass,
      VVMode,
      VVDecayMode
      );

  int const nEvents = chain.GetEntries();
  lastEvent = (nEvents < lastEvent) ? nEvents : lastEvent;
  int event_i = 0, actualEvent_i = 0;
  if (isVerbose)
    std::cout << (lastEvent - firstEvent + 1) << " event(s) will be processed."
      << std::endl;

  float sig, bsi, bkg, higgsMass;
  float sigRaw, bsiRaw, bkgRaw, cpsToBw;
  bool finalWeight = true;
  std::string sigHypo = "JJEW_SIG_ghv1_1_MCFM",
    bsiHypo = "JJEW_BSI_ghv1_1_MCFM", bkgHypo = "JJEW_BKG_MCFM";
  tree.Branch(sigHypo.c_str(), &sigRaw);
  tree.Branch(bsiHypo.c_str(), &bsiRaw);
  tree.Branch(bkgHypo.c_str(), &bkgRaw);
  tree.Branch("CPStoBWPropRewgt", &cpsToBw);
  tree.Branch("2l2nu_mass", &higgsMass);

  while(reader.Next()) {
    event_i++;

    if (event_i > lastEvent)
      break;
    if (event_i < firstEvent)
      continue;

    actualEvent_i++;

    if (isVerbose && not (actualEvent_i % 1000))
      std::cout << actualEvent_i << " events are processed. " << std::endl;
    // Get MELA final weights.
    sig = melaHandler.GetWeight(sigHypo.c_str(), "SIG");
    bsi = melaHandler.GetWeight(bsiHypo.c_str(), "BSI");
    bkg = melaHandler.GetWeight(bkgHypo.c_str(), "BKG");
    // Get MELA meps (weights computed by MELA)
    sigRaw = melaHandler.GetWeight(sigHypo.c_str(), "", not finalWeight);
    bsiRaw = melaHandler.GetWeight(bsiHypo.c_str(), "", not finalWeight);
    bkgRaw = melaHandler.GetWeight(bkgHypo.c_str(), "", not finalWeight);
    cpsToBw = melaHandler.GetWeight("CPStoBWPropRewgt", "", not finalWeight);

    higgsMass = melaHandler.GetHiggsMass();
    // continue if MELA was not able to compute a weights.
    if (sig < 0 or bsi < 0 or bkg < 0)
      continue;

    if (higgsMass >= 200) {
      massDistSig.Fill(higgsMass, sig);
      massDistBsi.Fill(higgsMass, bsi);
      massDistBkg.Fill(higgsMass, bkg);
    }

    tree.Fill();
    // After getting the weights, reset melaHandler in each event iteration.
    melaHandler.Reset();
  }

  if (isVerbose)
    std::cout << "Events are processed successfully." << std::endl;

  massDistSig.SetDirectory(&outputFile);
  massDistBsi.SetDirectory(&outputFile);
  massDistBkg.SetDirectory(&outputFile);
  outputFile.Write();
  outputFile.Close();
  return EXIT_SUCCESS;
}
